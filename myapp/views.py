from django.shortcuts import render
from django.http import HttpResponse , FileResponse
from datetime import datetime, date
#def home(request): #www.instagram.com/home
 #   return HttpResponse('Hello Home')


def home(request): #www.instagram.com/home
    time= datetime.now()
    text='hello'
    text1 = 'the first method'
    order_list= ['one','two','three']
    time_since = date(year=2018 , month=4 , day=20)
    time_till = date(year=2021, month=4, day=20)
    return render(request=request,
                  template_name= 'index.html',
                  context={'date':time,
                           'Text':text,
                           'Time_since': time_since,
                           'Time_till': time_till,
                           'Text1': text1,
                           'Order_list': order_list}
                  )





def image(request): #www.instagram.com/image
    return FileResponse(open('myapp/media/MVT_Image.png','rb'))

def audio(request): #www.instagram.com/audio
    return FileResponse(open('myapp/media/OmKalthoum.mp3','rb'))

def video(request): #www.instagram.com/audio
    return FileResponse(open('myapp/media/TheLittleMatchGirl.mp4','rb'))