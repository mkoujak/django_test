from django.db import models
from datetime import datetime


class Email_User(models.Model):
    email = models.EmailField(null=True)

class Food_List(models.Model):
    food = models.CharField(max_length=50,null=True)

class Book_List(models.Model):
    book = models.CharField(max_length=50,null=True)

class Client(models.Model):
    name = models.CharField(max_length=50, null=True, unique=True)
    email = models.OneToOneField(Email_User, on_delete=models.CASCADE)
    food = models.ForeignKey(Food_List, on_delete=models.CASCADE)
    book= models.ManyToManyField(Book_List)

    class Meta:
        db_table='mk_table'
        verbose_name='kunde'
        verbose_name_plural='kunden'
      #  ordering='name'
"""
class Student(models.Model):
    choseGender=(
        ('Male','Male'),
        ('Female', 'Female'),
        ('Other','Other'),
    )
    name = models.CharField(max_length=50, null=True,unique=True,blank=True,help_text='Enter Your Name')
    age = models.IntegerField()
    gender = models.CharField(max_length=6,null=True, default='Male', choices=choseGender)
    school = models.CharField(max_length=100, null=True,unique=True,error_messages={'unique':'اسم هذه المدرسة موجود من قبل'})
   # qe = models.BooleanField(default=True)

    agree = models.BooleanField(default=True)
    number=models.BigIntegerField()
    joined=models.DateTimeField()
    joinedDate = models.DateField(default=datetime.now)
    joinedTime = models.TimeField()

    email = models.EmailField(max_length=100,null=True)
    files = models.FileField(upload_to='myapp/media',null=True)
    images = models.ImageField(upload_to='myapp/media',null=True)
    text = models.textField(null=True)


    def __str__(self):
        return self.name
"""




