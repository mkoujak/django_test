
from django.urls import path
from myapp import views

urlpatterns = [
    path('home', views.home),
    path('image/', views.image),
    path('audio/', views.audio),
    path('video/', views.video),
]
